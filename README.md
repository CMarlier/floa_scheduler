Afin de récuperer la simulation sans bloquer le rendu, le javascript fait une requete fetch sur notre backend avec le montant de l'achat et la référence du mode de paiement choisi (3x, 4x, 10x),
notre serveur fait ensuite lui meme une requete de simulation via l'api Floa.
Enfin les données sont retournées et affichées avec les 3 fonctions floaSet{X}

Pour l'exemple nous avons mock les données retourné par notre serveur.

la doc de la route pour l'[order simulation](https://floapay.readme.io/reference/schedule-simulation)

On a essayer de retirer toutes les spécificités lié aux templates odoo pour que tu puisse au mieux l'adapter au ruby.
