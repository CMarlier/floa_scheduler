let mockData = {
  paymentOptionRef: "63",
  orderDate: "2024-06-08T11:06:32+01:00",
  amount: 5939,
  code: 0,
  message: "Request successfully processed",
  schedules: [
    {
      amount: 1487,
      date: "2024-06-08T11:06:32+01:00",
      rank: 1,
    },
    {
      amount: 1484,
      date: "2024-07-08T11:06:32+01:00",
      rank: 2,
    },
    {
      amount: 1484,
      date: "2024-08-08T11:06:32+01:00",
      rank: 3,
    },
    {
      amount: 1484,
      date: "2024-09-08T11:06:32+01:00",
      rank: 4,
    },
  ],
  feesAmount: null,
};

floaSetGauge(1234, mockData);
floaSetScheduler(1234, mockData);
floaSetTotal(1234, mockData);

function floaSetGauge(id, data) {
  $(`.floa-chart-gauge[data-id="${id}"]`).each(function () {
    var totalPayment = parseFloat(data.amount / 100);
    totalPayment =
      parseInt(totalPayment) == totalPayment
        ? parseInt(totalPayment)
        : totalPayment;
    var firstPayment = parseFloat(data.schedules[0].amount / 100);
    firstPayment =
      parseInt(firstPayment) == firstPayment
        ? parseInt(firstPayment)
        : firstPayment;
    JSC.chart($(this).attr("id"), {
      debug: false,
      legend_visible: false,
      defaultTooltip_enabled: false,
      xAxis_spacingPercentage: 0.6,
      width: 320,
      height: 320,
      box: {
        fill: "#ECECEC",
        padding: 0,
        margin: 0,
      },
      yAxis: [
        {
          id: "ax1",
          defaultTick: {
            padding: -10,
            enabled: false,
          },
          customTicks: [
            { value: 0, label_text: "0 €" },
            {
              value: totalPayment,
              label_text: `${totalPayment.toFixed(
                totalPayment % 1 == 0 ? 0 : 2,
              )} €`,
            },
          ],
          line: {
            width: 0,
            breaks: {},
          },
          scale_range: [0, totalPayment],
        },
      ],
      defaultSeries: {
        type: "gauge column roundcaps",
        shape: {
          label: {
            text: "%max €",
            align: "center",
            verticalAlign: "middle",
            style_fontSize: 28,
            style_fontWeight: 800,
          },
        },
      },
      series: [
        {
          type: "column roundcaps",
          name: "Temperatures",
          yAxis: "ax1",
          points: [["x", [0, firstPayment]]],
        },
      ],
    });
  });
}

function floaSetScheduler(id, data) {
  $(`.floa-payment-scheduler[data-id="${id}"]`).each(function () {
    var html = '<ul class="floa-scheduler-details">';
    data?.schedules?.forEach(function (elem, index) {
      html += '<li><div class="bullet"></div>';
      html += '<div class="w-100 d-flex justify-content-between">';
      html +=
        `<div class="${
          index === 0 ? "text-bold" : ""
        }">${toLocalDate(elem.date)}</div> <div class="${
          index === 0 ? "text-bold" : ""
        }">${(elem.amount / 100).toFixed(
          (elem.amount / 100) % 1 == 0 ? 0 : 2,
        )} €` + "</div></div></li>";
    });
    html += "</ul>";
    $(this).html(html);
  });
}

function floaSetTotal(id, data) {
  $(`.floa-payment-total[data-id="${id}"]`).each(function () {
    $(this)
      .find("div")
      .each(function (index, elem) {
        if (index == 0) {
          $(elem).html(
            $(elem).html() +
              `<span>${(data.amount / 100).toFixed(
                (data.amount / 100) % 1 == 0 ? 0 : 2,
              )} €</span>`,
          );
        } else {
          $(elem).html(
            $(elem).html() +
              `<span>${(data.feesAmount / 100).toFixed(
                (data.feesAmount / 100) % 1 == 0 ? 0 : 2,
              )} €</span>`,
          );
        }
      });
  });
}

function toLocalDate(date) {
  let rv = new Date(date);
  if (new Date().toISOString().slice(0, 10) === rv.toISOString().slice(0, 10))
    return "Aujourd'hui";
  return rv.toLocaleString("fr-fr", {
    day: "numeric",
    month: "long",
    year: "numeric",
  });
}
